'use strict';
angular.module('trz', [
    'oc.lazyLoad',
    'ngRoute',
    'trz.core.services'
])
        .run(['$rootScope', '$location', 'authService', function ($rootScope, $location, authService) {
                $rootScope.$on('$routeChangeStart', function (event) {

                    if (authService.isAuthenticated() || $location.path() === '/login') {
                        console.log('ALLOW');
                    } else {
                        console.log('DENY');
                        event.preventDefault();
                        $rootScope.$evalAsync(function () {
                            $location.path('/login');
                        });
                    }
                });
            }])



        .config(function ($routeProvider, $ocLazyLoadProvider, $httpProvider) {
            $httpProvider.interceptors.push('httpRequestInterceptor');
            $ocLazyLoadProvider.config({
                // main application module
                loadedModules: ['trz'],
                // defining lazy loaded modules and association to their dependencies
                modules: [{
                        name: 'trz.firms.controllers',
                        files: ['trz/firms/controllers/firmControllers.js',
                            'trz/firms/services/firmServices.js']
                    }, {
                        name: 'trz.login.controllers',
                        files: ['trz/login/controllers/loginControllers.js',
                            'trz/login/services/loginServices.js']
                    },
                ]});
            $routeProvider
                    .when('/login', {
                        templateUrl: 'trz/login/views/login.html',
                        controller: 'LoginController',
                        resolve: {
                            loadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load('trz.login.controllers');
                                }]
                        }
                    })
                    .when('/', {
                        templateUrl: 'trz/home/views/home.html'
                    })
                    .when('/firms/:firmId', {
                        templateUrl: 'trz/firms/views/firms.create.html',
                        controller: 'AddUpdateFirmsController',
                        resolve: {
                            loadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load('trz.firms.controllers');
                                }]
                        }
                    })
                    .when('/firms', {
                        templateUrl: 'trz/firms/views/firms.html',
                        controller: 'ListFirmsController',
                        resolve: {
                            loadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load('trz.firms.controllers');
                                }]
                        }
                    })
                    ;
        })
        .controller('MainCtrl', ['$route', '$routeParams', '$location',
            function ($route, $routeParams, $location) {

            }])
        .controller('HeaderCtrl', ['$scope', '$location', 'authService',
            function ($scope, $location, authService) {
                $scope.isAuthenticated = function(){
                    return authService.isAuthenticated();
                };
                $scope.logout = function(){
                    authService.removeAuthenticated();
                    $location.path('/login');
                };
            }])
        .filter('debug', function () {
            return function (input) {
                if (input === '')
                    return 'empty string';
                return input ? input : ('' + input);
            };
        })
        ;


