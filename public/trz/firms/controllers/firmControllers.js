angular.module('trz.firms.controllers', ['ngTable', 'ngRoute', 'trz.firms.services', 'ui.bootstrap', 'trz.core.modals'])
        .controller('AddUpdateFirmsController', ['$scope', '$routeParams', '$location', '$modal', 'firmFactory',
            function ($scope, $routeParams, $location, $modal, firmFactory) {
                var firmId = parseInt($routeParams.firmId);
                if (firmId > 0) {
                    firmFactory.readFirm(firmId)
                            .success(function (result) {
                                $scope.firm = result.data;
                            });
                }
                $scope.createFirm = function (firm) {
                    if (!$scope.firmForm.$valid) {
                        $scope.isSubmited = true;
                        return;
                    }

                    if (firm.id > 0) {
                        firmFactory.updateFirm(firm)
                                .success(function () {
                                    var modalInstance = $modal.open({
                                        templateUrl: 'trz/core/modals/message.html',
                                        controller: 'ModalMessageController',
                                        resolve: {
                                            data: function () {
                                                return {title: 'title', message: 'Фирмата беше добавена успешно'};
                                            }
                                        }
                                    });

                                    modalInstance.result.then(function () {
                                        $location.path('/firms');
                                    });
                                    //
                                })
                                .error(function () {
                                    $scope.firmForm.name.$error.required = true;
                                });
                    } else {
                        firmFactory.createFirm(firm)
                                .success(function () {
                                    var modalInstance = $modal.open({
                                        templateUrl: 'trz/core/modals/message.html',
                                        controller: 'ModalMessageController',
                                        resolve: {
                                            data: function () {
                                                return {title: 'title', message: 'Фирмата беше променена успешно'};
                                            }
                                        }
                                    });

                                    modalInstance.result.then(function () {
                                        $location.path('/firms');
                                    });
                                })
                                .error(function () {

                                });
                    }
                };


            }])
        .controller('ListFirmsController', ['$scope', '$route','$modal', 'firmFactory', 'ngTableParams',
            function ($scope, $route, $modal, firmFactory, ngTableParams) {
                firmFactory.firmsList()
                        .success(function (result) {
                            $scope.tableParams = new ngTableParams({
                                page: 1, // show first page
                                count: 10           // count per page
                            }, {
                                counts: [5, 10, 50],
                                getData: function ($defer, params) {
                                    params.total(result.data.length);
                                    $defer.resolve(result.data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                        });

                $scope.deleteFirm = function (id) {
                    firmFactory.deleteFirm(id)
                            .success(function (data) {
                                $route.reload();
                            })
                            .error(function (data) {
                                $modal.open({
                                    templateUrl: 'trz/core/modals/message.html',
                                    controller: 'ModalMessageController',
                                    resolve: {
                                        data: function () {
                                            return {title: 'Съобщение', message: 'Грешка'};
                                        }
                                    }
                                });
                            });
                };
            }])
        ;


