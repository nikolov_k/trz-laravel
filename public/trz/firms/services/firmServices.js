angular.module('trz.firms.services', [])

        .factory('firmFactory', function ($http) {
            return {
                createFirm: function (firm) {
                    return $http.post('http://trz-laravel.polivane.eu/firms', firm)

                },
                updateFirm: function (firm) {
                    return $http.put('http://trz-laravel.polivane.eu/firms/' + firm.id, firm)

                },
                readFirm: function (firmId) {
                    return $http.get('http://trz-laravel.polivane.eu/firms/' + firmId);
                },
                deleteFirm: function (firmId) {
                    return $http.delete('http://trz-laravel.polivane.eu/firms/' + firmId);
                },
                firmsList: function () {
                    return $http.get('http://trz-laravel.polivane.eu/firms');
                }
            };
        })
        ;


