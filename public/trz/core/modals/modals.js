angular.module('trz.core.modals',[])
        .controller('ModalMessageController', ['$scope', '$modalInstance', 'data',
            function ($scope, $modalInstance, data) {

                $scope.title = data.title;
                $scope.message = data.message;

                $scope.ok = function () {
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }])
        ;


