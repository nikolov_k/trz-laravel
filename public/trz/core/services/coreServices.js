angular.module('trz.core.services', [])

        .factory('authService', function () {
            var key = 'isAuthenticated';
            return{
                isAuthenticated: function () {
                    return localStorage.getItem(key) === 'true';
                },
                setAuthenticated: function () {
                    return localStorage.setItem(key, 'true');
                },
                removeAuthenticated: function () {
                    return localStorage.removeItem(key);
                }
            };
        })
        .factory('httpRequestInterceptor', function ($q, $location, authService) {
            return {
                'responseError': function (rejection) {
                    // do something on error
                    if (rejection.status === 401) {
                        authService.removeAuthenticated();
                        $location.path('/login');
                        return $q.reject(rejection);
                    }
                    
                    return $q.reject(rejection);
                }
            };
        });
;


