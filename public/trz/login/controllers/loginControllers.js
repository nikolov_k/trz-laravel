angular.module('trz.login.controllers', ['trz.login.services'])
        .controller('LoginController', ['$scope','$location', 'loginFactory', 'authService',
            function ($scope,$location, loginFactory, authService) {

                $scope.login = function (user)
                {
                    authService.removeAuthenticated();
                    $scope.errors = null;
                    loginFactory.login(user)
                            .success(function (data, status, headers, config) {
                                if (data.status === 'success') {
                                    authService.setAuthenticated();
                                    $location.path('/');
                                }
                            })
                            .error(function (data, status, headers, config) {
                                if (data && data.errors) {
                                    $scope.errors = data.errors;
                                }
                            });
                };
            }]);



