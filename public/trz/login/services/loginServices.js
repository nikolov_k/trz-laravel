angular.module('trz.login.services', [])

        .factory('loginFactory', function ($http) {
            return {
                login: function (user) {
                    return $http.post('http://trz-laravel.polivane.eu/api-login', user)

                }
            };
        })

        .factory('authService', function () {
            var key = 'isAuthenticated';
             return{
                isAuthenticated: function () {
                    return localStorage.getItem(key) === 'true';
                },
                setAuthenticated: function () {
                    return localStorage.setItem(key, 'true');
                },
                removeAuthenticated: function () {
                    return localStorage.removeItem(key);
                }
            }
        });




