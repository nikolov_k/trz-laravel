<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <header>
            @section('sidebar')
            <nav>
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    @if (Auth::guest())
                    <li><a href="{{ URL::to('login') }}">Login</a></li>
                    @else
                    <li><a href="{{ URL::to('firms/create') }}">Нова фирма</a></li>
                    <li><a href="{{ URL::to('logout') }}">Logout</a></li>
                    @endif
                </ul>
            </nav>
            @show
        </header>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
