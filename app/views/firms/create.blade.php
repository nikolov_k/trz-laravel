@extends('layouts.master')

@section('content')
<h1>Create User</h1>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

{{ Form::open(array('route' => 'firms.store')) }}
    <ul>

        <li>
            {{ Form::label('name', 'Име:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('eik', 'ЕИК:') }}
            {{ Form::text('eik') }}
        </li>

        <li>
            {{ Form::label('dds', 'ДДС Номер:') }}
            {{ Form::text('dds') }}
        </li>

        <li>
            {{ Form::label('district', 'Област:') }}
            {{ Form::text('district') }}
        </li>        

        <li>
            {{ Form::label('city', 'Град:') }}
            {{ Form::text('city') }}
        </li>

        <li>
            {{ Form::label('address', 'Адрес:') }}
            {{ Form::text('address') }}
        </li>
        
        <li>
            {{ Form::label('email', 'Емейл:') }}
            {{ Form::email('email') }}
        </li>

        <li>
            {{ Form::label('phone', 'Телефон:') }}
            {{ Form::text('phone') }}
        </li>
        
        <li>
            {{ Form::label('osig_kasa', 'Осигурителна каса:') }}
            {{ Form::checkbox('osig_kasa') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}


@stop