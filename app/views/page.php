 <!DOCTYPE html>

<html ng-app="trz">
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="script/ng-table/ng-table.css" rel="stylesheet" type="text/css"/>
        <link href="script/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>

    </head>
    <body ng-controller="MainCtrl">
        <div class="container">
            <div ng-include="'trz/core/views/header.html'"></div>
            <div ng-view></div>
            <div ng-include="'trz/core/views/footer.html'"></div>
        </div>
        <script src="script/angular.js" type="text/javascript"></script>
        <script src="script/angular-route.js" type="text/javascript"></script>
        <script src="script/ng-table/ng-table.js" type="text/javascript"></script>
        <script src="script/ocLazyLoad.js" type="text/javascript"></script>
        <script src="trz/app.js" type="text/javascript"></script>
        <script src="trz/core/services/coreServices.js" type="text/javascript"></script>
        <script src="trz/core/modals/modals.js" type="text/javascript"></script>
        <script src="script/ui-bootstrap-tpls-0.12.0.min.js" type="text/javascript"></script>
    </body>
</html>
