<!-- app/views/login.blade.php -->
@extends('layouts.master')

@section('content')
        {{ Form::open(array('url' => 'login')) }}
        <h1>Login</h1>

        <!-- if there are login errors, show them here -->
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
            {{ $errors->first('login') }}
        </p>

        <p>
            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', Input::old('email')) }}
        </p>

        <p>
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password') }}
        </p>

        <p>{{ Form::submit('Submit') }}</p>
        {{ Form::close() }}
@stop
