<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserService
 *
 * @author milla
 */
use Illuminate\Support\MessageBag;

class UsersService {

    /**
     * @var AbstractRepository 
     */
    protected $usersRepository;

    public function __construct(UsersRepository $usersRepository) {
        $this->usersRepository = $usersRepository;
    }

    public function login($email, $password) {

        $errors = new MessageBag();
        $userdata = array(
            'email' => $email,
            'password' => $password
        );
        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
// run the validation rules on the inputs from the form
        $validator = Validator::make($userdata, $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors->merge($validator->errors());
        } else {
            // attempt to do the login
            if (Auth::attempt($userdata)) {

                } else {
                                        
                // validation not successful, send back to form 
                $errors->add('login', 'Email and/or password invalid.');
            }
        }
        
        return $errors;
    }

}
