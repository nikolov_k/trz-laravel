<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FirmsService
 *
 * @author Krasimir
 */
use Illuminate\Support\MessageBag;

class FirmsService {

    private $firmsRepository;

    public function __construct(FirmsRepository $firmsRepository) {
        $this->firmsRepository = $firmsRepository;
    }

    public function createFirm(array $input) {
        $errors = new MessageBag();

        $firmdata = $this->mapInputToFirm($input);
        // validate the info, create rules for the inputs
        $rules = array(
            'name' => 'required', // make sure the email is an actual email
        );
// run the validation rules on the inputs from the form
        $validator = Validator::make($firmdata, $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors->merge($validator->errors());
            $data = null;
        } else {
            $data = $this->firmsRepository->create($firmdata);
        }

        return array(
            'errors' => $errors,
            'data' => $data
        );
    }
    
    public function deleteFirm($id) {
        return $this->firmsRepository->deleteByID($id);
    }
    
    public function updateFirm(array $input) {
        $errors = new MessageBag();

        $firmdata = $this->mapInputToFirm($input);
        // validate the info, create rules for the inputs
        $rules = array(
            'name' => 'required', // make sure the email is an actual email
        );
// run the validation rules on the inputs from the form
        $validator = Validator::make($firmdata, $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors->merge($validator->errors());
        } else {
            $result = $this->firmsRepository->updateFirm($firmdata);
        }

        return isset($result) ? $result : $errors;
    }

    public function getUserFirms() {
        $firms = $this->firmsRepository->getUserFirms(Auth::id());

        return $firms;
    }

    public function getUserFirm($id) {
        $firm = $this->firmsRepository->getByID($id);

        return $firm;
    }

    private function mapInputToFirm(array $input) {
        $firmdata = array(
            'id' => isset($input['id']) ? $input['id'] : 0,
            'name' => isset($input['name']) ? $input['name'] : '',
            'eik' => isset($input['eik']) ? $input['eik'] : '',
            'dds' => isset($input['dds']) ? $input['dds'] : '',
            'district' => isset($input['district']) ? $input['district'] : '',
            'city' => isset($input['city']) ? $input['city'] : '',
            'address' => isset($input['address']) ? $input['address'] : '',
            'email' => isset($input['email']) ? $input['email'] : '',
            'phone' => isset($input['phone']) ? $input['phone'] : '',
            'osig_kasa' => isset($input['osig_kasa']) ? $input['osig_kasa'] : false,
        );
        
        return $firmdata;
    }

}
