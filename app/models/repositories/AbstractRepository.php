<?php

/**
 * Description of AbstractRepository
 *
 * @author User
 */
abstract class AbstractRepository {

    /**
     * @var AbstractEntity
     */
    private $oEntity;

    public function getEntity() {
        return $this->oEntity;
    }

    public function setEntity($oEntity) {
        $this->oEntity = $oEntity;
    }

    public function __construct($sEntityName) {
        $this->setEntity(App::make($sEntityName));
    }

    public function getAll() {
        
        return $this->oEntity->all();
    }

    public function getByID($id) {
        return $this->getEntity()->findOrFail($id);
    }
    
    public function deleteByID($id) {
        $entity = $this->getEntity()->find($id);
        if ($entity != null) {
            return $this->getEntity()->find($id)->delete();
        } else  {
            return false;
        }
        
    }
    
    public function update(AbstractEntity $oAbstractEntity) {
        return $oAbstractEntity->save();
    }
    
    public function create(array $input) {
        
        return $this->oEntity->create($input);
    }
}
