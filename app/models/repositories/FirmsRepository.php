<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FirmsRepository
 *
 * @author Krasimir
 */
class FirmsRepository extends AbstractRepository {

    public function __construct() {
        parent::__construct('Firm');
    }

    public function create(array $firmdata) {

        $firm = new Firm();
        $firm->name = $firmdata['name'];
        $firm->user_id = Auth::id();
        $firm->eik = $firmdata['eik'];
        $firm->dds = $firmdata['dds'];
        $firm->district = $firmdata['district'];
        $firm->city = $firmdata['city'];
        $firm->address = $firmdata['address'];
        $firm->email = $firmdata['email'];
        $firm->phone = $firmdata['phone'];
        $firm->osig_kasa = $firmdata['osig_kasa'];
        return $firm->save();
    }
    
    public function updateFirm(array $firmdata) {

        $firm = Firm::findOrFail($firmdata['id']); 
        $firm->name = $firmdata['name'];
        $firm->user_id = Auth::id();
        $firm->eik = $firmdata['eik'];
        $firm->dds = $firmdata['dds'];
        $firm->district = $firmdata['district'];
        $firm->city = $firmdata['city'];
        $firm->address = $firmdata['address'];
        $firm->email = $firmdata['email'];
        $firm->phone = $firmdata['phone'];
        $firm->osig_kasa = $firmdata['osig_kasa'];
        return $firm->save();
    }

    public function getUserFirms($userId) {
        $firms = $this->getEntity()->where('user_id', '=', $userId)->get();
        
        return $firms;
    }
}
