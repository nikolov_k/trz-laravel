<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WorkersRepository
 *
 * @author Krasimir
 */
class WorkersRepository extends AbstractRepository {

    public function __construct() {
        parent::__construct('Worker');
    }

    public function create(array $workerdata) {

        $worker = new Firm();
        $worker->name = $workerdata['name'];
        $worker->firm_id = $workerdata['firm_id'];
        $worker->document_type = $workerdata['document_type'];
        $worker->document_number = $workerdata['document_number'];
        $worker->tzpb = $workerdata['tzpb'];
        $worker->job_type = $workerdata['job_type'];
        $worker->salary = $workerdata['salary'];
        $worker->mod_ = $workerdata['mod'];
        $worker->post_dobavki = $workerdata['post_dobavki'];
        $worker->start_date = $workerdata['start_date'];
        $worker->end_date = $workerdata['end_date'];
        $worker->zakon_rabotno_vreme = $workerdata['zakon_rabotno_vreme'];
        $worker->rabotno_vreme = $workerdata['rabotno_vreme'];
        return $worker->save();
    }
    
    public function updateFirm(array $workerdata) {

        $worker = Firm::findOrFail($workerdata['id']); 
        $worker->name = $workerdata['name'];
        $worker->document_type = $workerdata['document_type'];
        $worker->document_number = $workerdata['document_number'];
        $worker->tzpb = $workerdata['tzpb'];
        $worker->job_type = $workerdata['job_type'];
        $worker->salary = $workerdata['salary'];
        $worker->mod_ = $workerdata['mod'];
        $worker->post_dobavki = $workerdata['post_dobavki'];
        $worker->start_date = $workerdata['start_date'];
        $worker->end_date = $workerdata['end_date'];
        $worker->zakon_rabotno_vreme = $workerdata['zakon_rabotno_vreme'];
        $worker->rabotno_vreme = $workerdata['rabotno_vreme'];
        return $worker->save();
    }

    public function getUserFirms($userId) {
        $workers = $this->getEntity()->where('user_id', '=', $userId)->get();
        
        return $workers;
    }
}
