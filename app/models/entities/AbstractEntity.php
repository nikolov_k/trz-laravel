<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseEntity
 *
 * @author Krasimir
 */
abstract class AbstractEntity extends Eloquent {

    public $timestamps = false;

    public function getId() {
        return (int) $this->id;
    }

}
