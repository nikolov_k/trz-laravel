<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

//Route::controller('', 'HomeController');
// route to show the login form
//Route::get('login', array('uses' => 'HomeController@getLogin'));
//
//// route to process the form
//Route::post('login', array('uses' => 'HomeController@postLogin'));
Route::post('api-login', array('uses' => 'HomeController@postApiLogin'));
    Route::get('/', array('uses' => 'HomeController@getIndex'));

Route::group(array('before' => 'auth.api'), function() {

    Route::resource('firms', 'FirmsController');
});
