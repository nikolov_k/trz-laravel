<?php
use Illuminate\Support\MessageBag;

class HomeController extends BaseController {

    private $usersService;
    
    public function __construct(UsersService $usersService) {
        $this->usersService = $usersService;
    }

    public function getIndex() {
        
        //return View::make('home.index', array('name' => 'Krasi'));
        return View::make('page');
    }

    public function getLogin() {
        // show the form
        return View::make('home.login');
    }

    public function postLogin() {
        $errors = $this->usersService->login(Input::get('email'), Input::get('password'));
        if ($errors->isEmpty()) {
            return Redirect::to('/');
        } else {
            return Redirect::to('login')
                            ->withErrors($errors) // send back all errors to the login form
                            ->withInput(Input::except('password'));
        }
    }
    
    public function postApiLogin() {
        $errors = $this->usersService->login(Input::get('email'), Input::get('password'));
        if ($errors->isEmpty()) {
            return Response::json(array(
                'status' => 'success',
                'data' => Auth::user()
            ), 200);
        } else {
            return Response::json(array(
                'status' => 'error',
                'data' => null,
                'errors' => $errors
            ), 400);
        }
    }
    
    public function getLogout() {
            Auth::logout(); // log the user out of our application
    return Redirect::to('login'); // redirect the user to the login screen
    }

}
