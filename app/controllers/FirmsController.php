<?php

class FirmsController extends \BaseController {

    private $firmsService;

    public function __construct(FirmsService $firmsService) {
        $this->firmsService = $firmsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $firms = $this->firmsService->getUserFirms();

        return Response::json(array(
                    'status' => 'success',
                    'data' => $firms
                        ), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('firms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $result = $this->firmsService->createFirm(Input::all());
        if ($result['errors']->isEmpty()) {
            return Response::json(array(
                        'status' => 'success',
                        'data' => $result['data']
                            ), 200);
        } else {
            return Response::json(array(
                        'status' => 'error',
                        'data' => $result['data'],
                        'errors' => $result['errors']->getMessages()
                            ), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $firm = $this->firmsService->getUserFirm($id);

        return Response::json(array(
                    'status' => 'success',
                    'data' => $firm
                        ), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $result = $this->firmsService->updateFirm(Input::all());
        return Response::json(array(
                    'status' => 'success',
                    'data' => $result
                        ), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $result = $this->firmsService->deleteFirm($id);
        if ($result === true) {
            return Response::json(array(
                        'status' => 'success',
                        'data' => $result
                            ), 200);
        } else {
            return Response::json(array(
                        'status' => 'error',
                        'data' => $result
                            ), 400);
        }
    }

    public function missingMethod($parameters = array()) {
        throw new NotFoundHttpException("AAAAAAAAAAAAAAAAA.");
    }

}
